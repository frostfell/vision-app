//
//  RoundedShadowButton.swift
//  vision-app-dev
//
//  Created by Igor Chernyshov on 19.03.2018.
//  Copyright © 2018 Igor Chernyshov. All rights reserved.
//

import UIKit

class RoundedShadowButton: UIButton {

    override func awakeFromNib() {
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowRadius = 15
        self.layer.shadowOpacity = 0.75
        self.layer.cornerRadius = self.frame.height / 2
    }

}
